grails.project.work.dir = 'target'

grails.project.dependency.resolver = 'maven'
grails.project.dependency.resolution = {
    inherits 'global'
    log 'warn'

    repositories {
        grailsCentral()
        mavenLocal()
        mavenCentral()
    }

    dependencies {
        String jooqVersion = '3.5.2'
        compile "org.jooq:jooq:$jooqVersion"
        compile "org.jooq:jooq-meta:$jooqVersion"
        compile "org.jooq:jooq-codegen:$jooqVersion"
    }

    plugins {
        build(':release:3.0.1', ':rest-client-builder:2.0.3') {
            export = false
        }
    }
}
