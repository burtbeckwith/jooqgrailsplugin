includeTargets << grailsScript('_GrailsBootstrap')

target(jooqGenerateConfig: "Generating jooq config file.") {
    depends(compile)
    classLoader.loadClass("ie.uws.jooq.JooqRunner").newInstance().generateConfig()
}

setDefaultTarget(jooqGenerateConfig)
