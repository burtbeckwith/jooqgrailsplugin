includeTargets << grailsScript('_GrailsBootstrap')

target(jooqInit: "Initializes jOOQ environment based on configuration xml file(s).") {
    depends(classpath, checkVersion, configureProxy, compile, bootstrapOnce)
    classLoader.loadClass("ie.uws.jooq.JooqRunner").newInstance().init()
}

setDefaultTarget(jooqInit)
