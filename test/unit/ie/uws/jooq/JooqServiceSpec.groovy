package ie.uws.jooq

import grails.test.mixin.TestFor

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.jooq.DSLContext

import spock.lang.Specification

@TestFor(JooqService)
class JooqServiceSpec extends Specification {

    def setup() {
        JooqDSLContext.metaClass.static.getByDataSource = {GrailsApplication application, String dataSourceName ->
            if (dataSourceName == 'dataSource') {
                return [name: 'dslContext'] as DSLContext
            }
        }
    }

    void "getting contect for exisising dataSource should work"() {
        when:
        DSLContext context = service.dataSource

        then:
        notThrown Exception
    }

    void "getting contect for non-exisising dataSource should throw exception"() {
        when:
        DSLContext context = service.dataSourceUsers

        then:
        thrown(MissingPropertyException)
    }
}
