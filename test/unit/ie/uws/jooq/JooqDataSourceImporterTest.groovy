package ie.uws.jooq

import ie.uws.jooq.util.ConfigReader

import org.junit.Test

class JooqDataSourceImporterTest {

    @Test
    void generateXml() {
        ConfigReader.metaClass.read = { String className -> new ConfigObject() }
        ConfigReader.metaClass.static.mergeConfigFiles = { -> new ConfigObject() }
        JooqDataSourceImporter.metaClass.dataSource = {[
            driverClassName: 'drv',
            url: 'url',
            username: 'usr',
            password: 'pass'
        ]}
        JooqDataSourceImporter imp = new JooqDataSourceImporter("test")
        imp.metaClass.getDatabaseNameFromConnection = { -> "DB" }
        imp.metaClass.dialect = { -> "test" }
        assert imp.generateConfigFile() == '''\
<configuration xmlns='http://www.jooq.org/xsd/jooq-codegen-3.5.0.xsd'>
  <jdbc>
    <driver>drv</driver>
    <url>url</url>
    <user>usr</user>
    <password>pass</password>
  </jdbc>
  <generator>
    <name>org.jooq.util.DefaultGenerator</name>
    <database>
      <name>test</name>
<!-- <includes>DB.*</includes> -->
<!-- <inputSchema>DB</inputSchema> -->
    </database>
    <target>
      <packageName>jooq.test</packageName>
      <directory />
    </target>
  </generator>
</configuration>'''
    }
}
