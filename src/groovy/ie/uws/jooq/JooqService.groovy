package ie.uws.jooq

import org.jooq.DSLContext

class JooqService {

    def grailsApplication

    DSLContext getDataSource() {
        return propertyMissing('dataSource')
    }

    def propertyMissing(String propertyName) {
        DSLContext context = JooqDSLContext.getByDataSource(grailsApplication, propertyName)
        if (context) {
            return context
        }
        throw new MissingPropertyException(propertyName, JooqService)
    }
}
