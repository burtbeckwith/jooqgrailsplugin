package ie.uws.jooq

import ie.uws.jooq.util.ConfigReader

/**
 * @author Marcin Szlagor
 */
class JooqXmlConstructor {

    void generateConfigXml() {
        ConfigObject config = new ConfigReader().read(ConfigReader.CONFIG_CLASS)
        config.jooq.dataSource.each { String dataSourceName ->
            new JooqDataSourceImporter(dataSourceName).generateConfigXml()
            println "Generated config xml for ${dataSourceName}"
        }
    }
}
